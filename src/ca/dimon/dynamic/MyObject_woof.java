package ca.dimon.dynamic;

import org.apache.log4j.Logger;

public class MyObject_woof extends SomeOptoinalBaseClass implements AnInterface {

    // logger moved to super
    //public final Logger logger = Logger.getLogger(DynamicJavaClassLoadingRunner.class);

    public String bark() {
        // System.out.println("woof!"); //         syso-statements - no no!
        String message = "woof!";
        logger.info(message);
        return message;
    }

    //... body of class ... override superclass methods
    //    or implement interface methods
}
