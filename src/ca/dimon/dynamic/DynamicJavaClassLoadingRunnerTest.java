package ca.dimon.dynamic;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DynamicJavaClassLoadingRunnerTest extends junit.framework.TestCase {
//public class DynamicJavaClassLoadingRunnerTest {

//    public void testNothing() {
//    }

//    public void testWillAlwaysFail() {
//        fail("An error message");
//    }

    //@Test
    public void test_cat_voice() {
	MyObject_meow cat = new MyObject_meow();
	assertEquals("meow!", cat.bark());
    }

    //@Test
    public void test_dog_voice() {
	MyObject_woof dog = new MyObject_woof();
	assertEquals("woof!", dog.bark());
    }
}
