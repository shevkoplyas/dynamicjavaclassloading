package ca.dimon.dynamic;

import org.apache.log4j.Logger;

public class MyObject_meow extends SomeOptoinalBaseClass implements AnInterface {

    // logger moved to super
    //public final Logger logger = Logger.getLogger(DynamicJavaClassLoadingRunner.class);

    public String bark() {
        // System.out.println("meow!"); //         syso-statements - no no!
        String message = "meow!";
        logger.info(message);
        return message;
    }

    //... body of class ... override superclass methods
    //    or implement interface methods
}
