package ca.dimon.dynamic;

import java.io.IOException;
import java.io.File;

import org.apache.log4j.Logger;
//import org.apache.log4j.BasicConfigurator;

public class DynamicJavaClassLoadingRunner {

    private static Logger logger = Logger.getLogger(DynamicJavaClassLoadingRunner.class);

    public static void main(String[] args) throws
            ClassNotFoundException,
            IllegalAccessException,
            InstantiationException,
            InterruptedException,
            IOException {


        ClassLoader parentClassLoader = MyClassLoader.class.getClassLoader();
        MyClassLoader classLoader = new MyClassLoader(parentClassLoader);

        logger.info("loading class...");
        Class myObjectClass = classLoader.loadClass("ca.dimon.dynamic.MyObject_woof");
        AnInterface object1 = (AnInterface) myObjectClass.newInstance();

        logger.info("calling bark():");
        object1.bark();




        logger.info("re-loading another implementation of the same class from other file...");

        //create new class loader so classes can be reloaded.
        classLoader = new MyClassLoader(parentClassLoader);

        //myObjectClass = classLoader.loadClass("MyObject");
        myObjectClass = classLoader.loadClass("ca.dimon.dynamic.MyObject_meow");

        object1 = (AnInterface) myObjectClass.newInstance();

        logger.info("calling bark():");
        object1.bark();

    }
}
