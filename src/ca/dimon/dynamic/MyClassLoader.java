package ca.dimon.dynamic;

import java.lang.ClassLoader;
import java.net.URL;
import java.net.URLConnection;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.io.IOException;

public class MyClassLoader extends ClassLoader {

    public MyClassLoader(ClassLoader parent) {
        super(parent);
    }

    public Class loadClass(String name) throws ClassNotFoundException {
        if (!name.matches("(.*)MyObject(.*)")) {
	    System.out.println("->->->-> fwd call to super loader.. name: '" + name + "'");
            return super.loadClass(name);
        }

        try {
            String name_fs = name.replaceAll("\\.", "/");
            String url = "file:./build/classes/" + name_fs + ".class";
	    System.out.println("-!-!-!-! custom class load: name_fs: '" + name_fs + "'");

            URL myUrl = new URL(url);
            URLConnection connection = myUrl.openConnection();
            InputStream input = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int data = input.read();

            while (data != -1) {
                buffer.write(data);
                data = input.read();
            }
	    System.out.println("read " + buffer.size() + " bytes from file: '" + name_fs + "'");

            input.close();

            byte[] classData = buffer.toByteArray();

            return defineClass(name,
                    classData, 0, classData.length);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
