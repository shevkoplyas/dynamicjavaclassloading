
This project is just an implementation of java dynamic class loading based on Jakob Jenkov's article:
    http://tutorials.jenkov.com/java-reflection/dynamic-class-loading-reloading.html

How to run:
    git clone http://bitbucket.org/shevkoplyas/dynamicjavaclassloading.git
    cd dynamicjavaclassloading/
    ant

Other related/useful links:

ant tutorial:
    https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html

junit notes:
    Getting started: https://github.com/junit-team/junit/wiki/Getting-started
    Download the current junit-4.XX.jar from JUnit's release page:
    https://github.com/junit-team/junit/releases

logger notes:
    http://stackoverflow.com/questions/7624895/how-to-use-log4j-with-multiple-classes
    https://logging.apache.org/log4j/1.2/apidocs/org/apache/log4j/PatternLayout.html
    %l - The location information can be very useful. However, its generation is extremely slow and should be avoided unless execution speed is not an issue.

contact me:
    "Dmitry Shevkoplyas" <shevkoplyas[ at] Gmai1-dot-c0m>
